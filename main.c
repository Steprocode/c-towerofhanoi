// Created by Maciej Stępień

#include "primlib.h"
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <stdbool.h>

#define BACKGROUND_COLOR BLUE
#define PEG_COLOR YELLOW
#define BLOCK_COLOR RED
#define GROUND_COLOR GREEN
#define TEXT_COLOR WHITE

#define BLOCKS 10
#define PEGS 4
#define PEG_WIDTH 10
#define BLOCK_HEIGHT 50
#define UPLIFT_LEVEL 50
#define GROUND_HEIGHT 50
#define DELAY_TIME 10
#define STEP 5


enum BlockState {TOP, UPLIFT, TRANSITION, DOWNLIFT, DOWN};


typedef struct {
    int x;
    int y;
    int width;
    int height;
    enum color color;
} Rectangle;


typedef struct {
    Rectangle rectangle;
    int pegNumber;
    int stackNumber;
} Block;


typedef struct {
    Block *block;
    enum BlockState blockState;
    int destination;
} Action;


Action action;
bool firstRun = true;


Rectangle createRectangle(int x, int y, int width, int height, enum color color)
{
    Rectangle rectangle;
    rectangle.x = x;
    rectangle.y = y;
    rectangle.width = width;
    rectangle.height = height;
    rectangle.color = color;

    return rectangle;
}


void drawRectangle(Rectangle rectangle)
{
    gfx_filledRect(rectangle.x,
                   rectangle.y - rectangle.height,
                   rectangle.x + rectangle.width,
                   rectangle.y,
                   rectangle.color);
}


Block createBlock(int pegNumber, int stackNumber, Rectangle rectangle)
{
    Block block;
    block.pegNumber = pegNumber;
    block.stackNumber = stackNumber;
    block.rectangle = rectangle;
    return block;
}


void spawnBlocks(int spacing, int groundLevel, Block blocks[BLOCKS])
{
    for (int blockIndex = 0; blockIndex < BLOCKS; blockIndex++)
    {
        int blockWidth = (spacing - spacing / BLOCKS * blockIndex) * 2;
        blocks[blockIndex] = createBlock(0, blockIndex, createRectangle(spacing - blockWidth / 2,
                                         groundLevel - BLOCK_HEIGHT * blockIndex,
                                         blockWidth,
                                         BLOCK_HEIGHT,
                                         BLOCK_COLOR));
    }
}


void spawnPegs(int pegSpacing, Rectangle pegs[PEGS], Rectangle ground)
{
    for (int pegIndex = 0; pegIndex < PEGS; pegIndex++)
    {
        pegs[pegIndex] = createRectangle(pegSpacing * (pegIndex * 2 + 1) - PEG_WIDTH / 2,
                                         ground.y - ground.height,
                                         PEG_WIDTH,
                                         BLOCK_HEIGHT * (BLOCKS + 1),
                                         PEG_COLOR);
    }
}


void drawScene(Rectangle pegs[PEGS], Block blocks[BLOCKS])
{
    static Rectangle ground;
    static int pegSpacing;

    if (firstRun)
    {
        pegSpacing = gfx_screenWidth() / (PEGS * 2);
        ground = createRectangle(0, gfx_screenHeight(), gfx_screenWidth(), GROUND_HEIGHT, GROUND_COLOR);
        spawnPegs(pegSpacing, pegs, ground);
        spawnBlocks(pegSpacing, ground.y - ground.height, blocks);
        firstRun = false;
    }

    for (int pegIndex = 0; pegIndex < PEGS; pegIndex++)
        drawRectangle(pegs[pegIndex]);
    for (int blockIndex = 0; blockIndex < BLOCKS; blockIndex++)
        drawRectangle(blocks[blockIndex].rectangle);
    drawRectangle(ground);
}


Block *getTopBlock(int peg, Block blocks[BLOCKS])
{
    Block *block = NULL;
    for (int blockIndex = 0; blockIndex < BLOCKS; blockIndex++)
    {
        if (blocks[blockIndex].pegNumber == peg)
        {
            if (block == NULL)
                block = &blocks[blockIndex];
            else if ((*block).stackNumber < blocks[blockIndex].stackNumber)
                block = &blocks[blockIndex];
        }
    }
    return block;
}


void animateUplift(Rectangle pegs[PEGS])
{
    int targetY = pegs[0].y - pegs[0].height - UPLIFT_LEVEL;
    if ((*action.block).rectangle.y > targetY)
    {
        (*action.block).rectangle.y -= STEP;
    }
    else
    {
        (*action.block).rectangle.y = targetY;
        (*action.block).pegNumber = -1;
        action.blockState = TOP;
    }
}


void animateDownlift(Block blocks[BLOCKS])
{
    int targetY = gfx_screenHeight() - GROUND_HEIGHT;
    Block *topBlock = getTopBlock(action.destination, blocks);
    if (topBlock != NULL)
        targetY = (*topBlock).rectangle.y - (*topBlock).rectangle.height;

    if ((*action.block).rectangle.y < targetY)
    {
        (*action.block).rectangle.y += STEP;
    }
    else
    {
        (*action.block).rectangle.y = targetY;
        (*action.block).pegNumber = action.destination;
        (*action.block).stackNumber = 0;
        if (topBlock != NULL)
            (*action.block).stackNumber = (*topBlock).stackNumber + 1;
        action.blockState = DOWN;
    }
}


void animateTransition(Rectangle pegs[PEGS])
{
    int targetX = pegs[action.destination].x + pegs[action.destination].width / 2;
    int blockXPos = (*action.block).rectangle.x + (*action.block).rectangle.width / 2;
    int sign = 1;
    if (targetX - blockXPos < 0)
        sign = -1;
    if ((sign == 1 && blockXPos < (targetX - STEP))
            || (sign == -1 && blockXPos > (targetX + STEP)))
    {
        (*action.block).rectangle.x += STEP * sign;
    }
    else
    {
        (*action.block).rectangle.x = targetX - (*action.block).rectangle.width / 2;
        action.blockState = DOWNLIFT;
    }
}


bool updateAnimation(Rectangle pegs[PEGS], Block blocks[BLOCKS])
{
    if(action.blockState == UPLIFT)
    {
        animateUplift(pegs);
        return true;
    }
    else if (action.blockState == DOWNLIFT)
    {
        animateDownlift(blocks);
        return true;
    }
    else if(action.blockState == TRANSITION)
    {
        animateTransition(pegs);
        return true;
    }
    return false;
}


void performAction(int number, Block blocks[BLOCKS])
{
    if (number >= PEGS)
        return;

    if (action.blockState == DOWN && getTopBlock(number, blocks) != NULL)
    {
        action.blockState = UPLIFT;
        action.destination = number;
        action.block = getTopBlock(number, blocks);
    }
    else if (action.blockState == TOP)
    {
        Block *topBlock = getTopBlock(number, blocks);
        if (topBlock == NULL || (topBlock != NULL && (*topBlock).rectangle.width > (*action.block).rectangle.width))
        {
            action.blockState = TRANSITION;
            action.destination = number;
        }
    }
}


void handleInput(bool gameOver, Block blocks[BLOCKS])
{
    int key = gfx_pollkey();
    switch(key)
    {
    case(SDLK_1):
        performAction(0, blocks);
        break;
    case(SDLK_2):
        performAction(1, blocks);
        break;
    case(SDLK_3):
        performAction(2, blocks);
        break;
    case(SDLK_4):
        performAction(3, blocks);
        break;
    case(SDLK_5):
        performAction(4, blocks);
        break;
    case(SDLK_6):
        performAction(5, blocks);
        break;
    case(SDLK_7):
        performAction(6, blocks);
        break;
    case(SDLK_8):
        performAction(7, blocks);
        break;
    case(SDLK_9):
        performAction(8, blocks);
        break;
    case(SDLK_ESCAPE):
        exit(0);
        break;
    case(SDLK_RETURN):
        if (gameOver)
            firstRun = true;
        break;
    }

}


bool isGameOver(Block blocks[BLOCKS])
{
    for (int blockIndex = 0; blockIndex < BLOCKS; blockIndex++)
        if (blocks[blockIndex].pegNumber != PEGS - 1)
            return false;

    return true;
}


void renderGameOver()
{
    gfx_textout(300, 300, "Congratulations! Game Over!", TEXT_COLOR);
    gfx_textout(300, 350, "Press ENTER to play again.", TEXT_COLOR);
    gfx_textout(300, 400, "Press ESC to exit the game.", TEXT_COLOR);
}


int main(int argc, char *argv[]) {
    if (gfx_init())
        exit(3);

    Block blocks[BLOCKS];
    Rectangle pegs[PEGS];
    bool running = true;
    bool gameOver;



    while (running) {
        if (firstRun)
        {
            gameOver = false;
            action.blockState = DOWN;
        }

        gfx_filledRect(0, 0, gfx_screenWidth() - 1, gfx_screenHeight() - 1, BACKGROUND_COLOR);
        drawScene(pegs, blocks);
        handleInput(gameOver, blocks);

        if (!gameOver)
        {
            updateAnimation(pegs, blocks);
            gameOver = isGameOver(blocks);
            firstRun = false;
        }
        else
            renderGameOver();

        gfx_updateScreen();
        SDL_Delay(DELAY_TIME);
    }

    gfx_getkey();
    return 0;
}
