all: main

main: main.o primlib.o
	gcc -fsanitize=undefined -g $^ -o $@ -lSDL2 -lSDL2_gfx `sdl2-config --libs` -lm

.c.o:
	gcc -fsanitize=undefined -g -Wall -pedantic `sdl2-config --cflags` -c  $< -lm 

primlib.o: primlib.c primlib.h

main.o: main.c primlib.h

clean:
	-rm primlib.o main.o main
