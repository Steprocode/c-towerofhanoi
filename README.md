# C-TowerOfHanoi
A simple Tower of Hanoi game written in C using SDL2.

## Table of contents
* [Description](#description)
* [Dependencies](#dependencies)
* [Setup](#setup)
	
## Description
The purpose of this game is to move all blocks to the last pile. A player can select a block from a pile by pressing '1', '2', '3' or '4' key and then select the target pile in the same way. Only smaller block can be placed on a bigger one.

## Dependencies
Program requires following libraries to be installed:

* SDL2-devel (providing SDL2/SDL.h)
* SDL2_gfx-devel (providing SDL2/SDL2_gfxPrimitives.h)
	
## Setup
To compile this project use Make:
```
$ make
```

Run the compiled program:
```
$ ./main
```

To remove files after compilation:
```
$ make clean
```
